//
//  VSTrack.m
//  Last.fm Helper
//
//  Created by Vladislav Solovyov on 16.12.15.
//  Copyright © 2015 Astarus. All rights reserved.
//

#import "VSTrack.h"

@implementation VSTrack

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{ @"name": @"name" };
}

@end
