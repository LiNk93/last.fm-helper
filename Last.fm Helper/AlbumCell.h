//
//  AlbumCell.h
//  Last.fm Helper
//
//  Created by Vladislav Solovyov on 16.12.15.
//  Copyright © 2015 Astarus. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlbumCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *albumImageView;
@property (weak, nonatomic) IBOutlet UILabel *albumNameLabel;

@end
