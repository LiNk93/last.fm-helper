//
//  AlbumSongsViewController.h
//  Last.fm Helper
//
//  Created by Vladislav Solovyov on 16.12.15.
//  Copyright © 2015 Astarus. All rights reserved.
//

#import <UIKit/UIKit.h>

@class VSAlbum;

@interface AlbumSongsViewController : UIViewController

@property (nonatomic) VSAlbum *album;

@end
