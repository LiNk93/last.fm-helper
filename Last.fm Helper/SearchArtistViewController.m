//
//  MasterViewController.m
//  Last.fm Helper
//
//  Created by Vladislav Solovyov on 15.12.15.
//  Copyright © 2015 Astarus. All rights reserved.
//

#import "SearchArtistViewController.h"
#import "ArtistTopAlbumsViewController.h"

#import "VSNetworkManager.h"
#import "LGAlertView.h"

#import "VSArtist.h"

@interface SearchArtistViewController ()

@property NSMutableArray *artists;

@end

@implementation SearchArtistViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.artistTopAlbumsViewController = (ArtistTopAlbumsViewController *)[[self.splitViewController.viewControllers lastObject] topViewController];
    
    self.artists = [NSMutableArray new];
}

- (void)viewWillAppear:(BOOL)animated {
    self.clearsSelectionOnViewWillAppear = self.splitViewController.isCollapsed;
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showDetail"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        ArtistTopAlbumsViewController *topAlbumsController = (ArtistTopAlbumsViewController *)[[segue destinationViewController] topViewController];
        VSArtist *artist = (VSArtist *)self.artists[indexPath.row];
        [topAlbumsController setArtist:artist];
    }
}


#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.artists.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *artistCell = [tableView dequeueReusableCellWithIdentifier:@"ArtistCell" forIndexPath:indexPath];

    VSArtist *artist = (VSArtist *)self.artists[indexPath.row];
    artistCell.textLabel.text = [artist name];
    return artistCell;
}


#pragma mark - UISearchBarDelegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    if (searchText.length) {
        [self performSearchRequestWithSearchText:searchText];
    } else {
        [self.artists removeAllObjects];
        [self.tableView reloadData];
    }
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self performSearchRequestWithSearchText:searchBar.text];
}


#pragma mark - Helpers

- (void)performSearchRequestWithSearchText:(NSString *)searchText {
    SearchArtistViewController __weak * weakSelf = self;
    [[VSNetworkManager sharedAPI] getSearchResultsFor:searchText withSuccess:^(NSArray *searchResults) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf.artists removeAllObjects];
            weakSelf.artists = [NSMutableArray arrayWithArray:searchResults];
            [weakSelf.tableView reloadData];
        });
    } failure:^(NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            LGAlertView *allert = [[LGAlertView alloc] initWithTitle:@"Ошибка"
                                                             message:error.localizedDescription
                                                               style:LGAlertViewStyleAlert
                                                        buttonTitles:@[ @"ОК" ]
                                                   cancelButtonTitle:nil
                                              destructiveButtonTitle:nil
                                                       actionHandler:nil
                                                       cancelHandler:nil
                                                  destructiveHandler:nil];
            [allert showAnimated:YES completionHandler:nil];
        });
    }];
}

@end
