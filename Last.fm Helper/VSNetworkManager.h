//
//  VSNetworkManager.h
//  Last.fm Helper
//
//  Created by Vladislav Solovyov on 15.12.15.
//  Copyright © 2015 Astarus. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking.h>

@class VSArtist, VSAlbum;

typedef void(^FailWithErrorBlock)(NSError *error);

@interface VSNetworkManager :  AFHTTPSessionManager

#pragma mark - Lifespan

+ (VSNetworkManager *)sharedAPI;


#pragma mark - Search

- (void)getSearchResultsFor:(NSString *)artist withSuccess:(void (^)(NSArray *searchResults))success failure:(FailWithErrorBlock)failure;


#pragma mark - Top Albums

- (void)getTopAlbumsForArtist:(VSArtist *)artist withSuccess:(void (^)(NSArray *topAlbums))success failure:(FailWithErrorBlock)failure;

#pragma mark - Top Albums

- (void)getTracksInAlbum:(VSAlbum *)album ofArtist:(VSArtist *)artist withSuccess:(void (^)(NSArray *tracks))success failure:(FailWithErrorBlock)failure;

@end