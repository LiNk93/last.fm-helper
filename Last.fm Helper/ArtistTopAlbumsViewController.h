//
//  DetailViewController.h
//  Last.fm Helper
//
//  Created by Vladislav Solovyov on 15.12.15.
//  Copyright © 2015 Astarus. All rights reserved.
//

#import <UIKit/UIKit.h>

@class VSArtist;

@interface ArtistTopAlbumsViewController : UIViewController

@property (nonatomic) VSArtist *artist;

@end

