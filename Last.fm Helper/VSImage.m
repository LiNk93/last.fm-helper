//
//  VSImage.m
//  Last.fm Helper
//
//  Created by Vladislav Solovyov on 16.12.15.
//  Copyright © 2015 Astarus. All rights reserved.
//

#import "VSImage.h"

@implementation VSImage

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{@"size": @"size",
             @"imageUrl": @"#text"};
}

+ (NSValueTransformer *)urlJSONTransformer {
    return [MTLValueTransformer transformerUsingForwardBlock:^id (NSString *str, BOOL *success, NSError *__autoreleasing *error){
        NSString *decodedString = [str stringByRemovingPercentEncoding];
        return [NSURL URLWithString:decodedString];
    } reverseBlock:^id (NSURL *url, BOOL *success, NSError *__autoreleasing *error){
        NSString *urlStr = [url absoluteString];
        NSString *encodedString = [urlStr stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
        return encodedString;
    }];
}

+ (NSValueTransformer *)imageUrlJSONTransformer {
    return [NSValueTransformer valueTransformerForName:MTLURLValueTransformerName];
}

@end
