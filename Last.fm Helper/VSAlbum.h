//
//  VSAlbum.h
//  Last.fm Helper
//
//  Created by Vladislav Solovyov on 16.12.15.
//  Copyright © 2015 Astarus. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Mantle/Mantle.h>
#import "MTLModel.h"

@class VSArtist;

@interface VSAlbum : MTLModel <MTLJSONSerializing>

@property (copy, nonatomic) NSString *name;
@property (copy, nonatomic) NSURL *albumUrl;
@property (copy, nonatomic) NSArray *images;
@property (copy, nonatomic) VSArtist *artist;
@property (copy, nonatomic) NSString *mbid;

@property (nonatomic) NSMutableArray *tracks;

@end
