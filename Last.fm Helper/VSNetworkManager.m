//
//  VSNetworkManager.m
//  Last.fm Helper
//
//  Created by Vladislav Solovyov on 15.12.15.
//  Copyright © 2015 Astarus. All rights reserved.
//

#import "VSNetworkManager.h"

#import "VSArtist.h"
#import "VSAlbum.h"
#import "VSTrack.h"

// Constants
static NSString *testServerUrl = @"http://foxdie.pythonanywhere.com/";

@implementation VSNetworkManager

#pragma mark - Lifespan

+ (VSNetworkManager *)sharedAPI {
    static VSNetworkManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[VSNetworkManager alloc] initWithBaseURL:[NSURL URLWithString:testServerUrl]];
    });
    
    return sharedInstance;
}

- (instancetype)initWithBaseURL:(NSURL *)url
{
    self = [super initWithBaseURL:url];
    
    if (self) {
        self.responseSerializer = [AFJSONResponseSerializer serializer];
        self.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html", @"application/json", nil];
        [self.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
        VSNetworkManager __weak * weakSelf = self;
        [self.reachabilityManager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
            switch (status) {
                case AFNetworkReachabilityStatusReachableViaWWAN:
                case AFNetworkReachabilityStatusReachableViaWiFi:
                    [weakSelf.operationQueue setSuspended:NO];
                    break;
                case AFNetworkReachabilityStatusNotReachable:
                default:
                    [weakSelf.operationQueue setSuspended:YES];
                    break;
            }
        }];
    }
    
    return self;
}


#pragma mark - Search

- (void)getSearchResultsFor:(NSString *)artist withSuccess:(void (^)(NSArray *searchResults))success failure:(FailWithErrorBlock)failure {
    [self GET:@"artist.search"
   parameters:@{ @"artist": artist }
     progress:nil
      success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
          NSDictionary *responseJSON = (NSDictionary *)responseObject;
          NSError *mantleError;
          NSArray *searchResults = [MTLJSONAdapter modelsOfClass:[VSArtist class]
                                                   fromJSONArray:responseJSON[@"results"][@"artistmatches"][@"artist"]
                                                           error:&mantleError];
          if (!mantleError) {
              success(searchResults);
          } else {
              failure(mantleError);
          }
      }
      failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
          failure(error);
      }];
}


#pragma mark - Top Albums

- (void)getTopAlbumsForArtist:(VSArtist *)artist withSuccess:(void (^)(NSArray *topAlbums))success failure:(FailWithErrorBlock)failure {
    [self GET:@"artist.getTopAlbums"
   parameters:@{ @"artist": artist.name }
     progress:nil
      success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
          NSDictionary *responseJSON = (NSDictionary *)responseObject;
          NSError *mantleError;
          NSArray *topAlbums = [MTLJSONAdapter modelsOfClass:[VSAlbum class]
                                               fromJSONArray:responseJSON[@"topalbums"][@"album"]
                                                       error:&mantleError];
          if (!mantleError) {
              success(topAlbums);
          } else {
              failure(mantleError);
          }
      }
      failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
          failure(error);
      }];
}

#pragma mark - Top Albums

- (void)getTracksInAlbum:(VSAlbum *)album ofArtist:(VSArtist *)artist withSuccess:(void (^)(NSArray *tracks))success failure:(FailWithErrorBlock)failure {
    [self GET:@"album.getInfo"
   parameters:@{ @"artist": artist.name,
                 @"album": album.name }
     progress:nil
      success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
          NSDictionary *responseJSON = (NSDictionary *)responseObject;
          NSError *mantleError;
          NSArray *tracks = [MTLJSONAdapter modelsOfClass:[VSTrack class]
                                            fromJSONArray:responseJSON[@"album"][@"tracks"][@"track"]
                                                    error:&mantleError];
          if (!mantleError) {
              success(tracks);
          } else {
              failure(mantleError);
          }
      }
      failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
          failure(error);
      }];
}

@end


