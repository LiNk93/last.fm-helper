//
//  AlbumSongsViewController.m
//  Last.fm Helper
//
//  Created by Vladislav Solovyov on 16.12.15.
//  Copyright © 2015 Astarus. All rights reserved.
//

#import "AlbumSongsViewController.h"
#import "VSNetworkManager.h"
#import "LGAlertView.h"
#import "VSAlbum.h"
#import "VSArtist.h"
#import "VSTrack.h"

@interface AlbumSongsViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@end

@implementation AlbumSongsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.navigationItem.title = self.album.name;
    AlbumSongsViewController __weak * weakSelf = self;
    [self startLoading];
    [[VSNetworkManager sharedAPI] getTracksInAlbum:self.album ofArtist:self.album.artist withSuccess:^(NSArray *tracks) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf stopLoading];
            weakSelf.album.tracks = [NSMutableArray arrayWithArray:tracks];
            [weakSelf.tableView reloadData];
        });
    } failure:^(NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf stopLoading];
            LGAlertView *allert = [[LGAlertView alloc] initWithTitle:@"Ошибка"
                                                             message:error.localizedDescription
                                                               style:LGAlertViewStyleAlert
                                                        buttonTitles:@[ @"ОК" ]
                                                   cancelButtonTitle:nil
                                              destructiveButtonTitle:nil
                                                       actionHandler:nil
                                                       cancelHandler:nil
                                                  destructiveHandler:nil];
            [allert showAnimated:YES completionHandler:nil];
        });
    }];
}

- (void)startLoading {
    self.tableView.alpha = 0;
    self.tableView.userInteractionEnabled = NO;
    [self.activityIndicator startAnimating];
    self.activityIndicator.alpha = 1;
}

- (void)stopLoading {
    self.tableView.alpha = 1;
    self.tableView.userInteractionEnabled = YES;
    [self.activityIndicator stopAnimating];
    self.activityIndicator.alpha = 0;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.album.tracks.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *trackCell = [tableView dequeueReusableCellWithIdentifier:@"TrackCell" forIndexPath:indexPath];
    VSTrack *track = (VSTrack *)self.album.tracks[indexPath.row];
    trackCell.textLabel.text = [track name];
    return trackCell;
}

@end
