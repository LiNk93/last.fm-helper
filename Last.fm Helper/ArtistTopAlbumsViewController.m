//
//  DetailViewController.m
//  Last.fm Helper
//
//  Created by Vladislav Solovyov on 15.12.15.
//  Copyright © 2015 Astarus. All rights reserved.
//

#import "ArtistTopAlbumsViewController.h"
#import "VSNetworkManager.h"
#import "LGAlertView.h"
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"
#import "AlbumCell.h"
#import "AlbumSongsViewController.h"

#import "VSArtist.h"
#import "VSAlbum.h"
#import "VSImage.h"

@interface ArtistTopAlbumsViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property NSMutableArray *topAlbums;

@end

@implementation ArtistTopAlbumsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.topAlbums = [NSMutableArray new];
    ArtistTopAlbumsViewController __weak * weakSelf = self;
    [self startLoading];
    [[VSNetworkManager sharedAPI] getTopAlbumsForArtist:self.artist withSuccess:^(NSArray *topAlbums) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf stopLoading];
            [weakSelf.topAlbums removeAllObjects];
            weakSelf.topAlbums = [NSMutableArray arrayWithArray:topAlbums];
            [weakSelf.tableView reloadData];
        });
    } failure:^(NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf stopLoading];
            LGAlertView *allert = [[LGAlertView alloc] initWithTitle:@"Ошибка"
                                                             message:error.localizedDescription
                                                               style:LGAlertViewStyleAlert
                                                        buttonTitles:@[ @"ОК" ]
                                                   cancelButtonTitle:nil
                                              destructiveButtonTitle:nil
                                                       actionHandler:nil
                                                       cancelHandler:nil
                                                  destructiveHandler:nil];
            [allert showAnimated:YES completionHandler:nil];
        });
    }];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}

- (void)startLoading {
    self.tableView.alpha = 0;
    self.tableView.userInteractionEnabled = NO;
    [self.activityIndicator startAnimating];
    self.activityIndicator.alpha = 1;
}

- (void)stopLoading {
    self.tableView.alpha = 1;
    self.tableView.userInteractionEnabled = YES;
    [self.activityIndicator stopAnimating];
    self.activityIndicator.alpha = 0;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showTracks"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        VSAlbum *album = (VSAlbum *)self.topAlbums[indexPath.row];
        AlbumSongsViewController *albumSongsVC = segue.destinationViewController;
        albumSongsVC.album = album;
    }
}

#pragma mark - Table View

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 120.f;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.topAlbums.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    AlbumCell *albumCell = [tableView dequeueReusableCellWithIdentifier:@"AlbubCell" forIndexPath:indexPath];
    VSAlbum *album = (VSAlbum *)self.topAlbums[indexPath.row];
    albumCell.albumNameLabel.text = [album name];
    AlbumCell __weak * weakAlbumCell = albumCell;
    [albumCell.albumImageView setImageWithURL:[album.images[3] imageUrl]
                                    completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
                                        dispatch_async(dispatch_get_main_queue(), ^{
                                            if (image && cacheType == SDImageCacheTypeNone) {
                                                weakAlbumCell.albumImageView.alpha = 0;
                                                [UIView animateWithDuration:0.6
                                                                 animations:^{
                                                                     weakAlbumCell.albumImageView.alpha = 1.0;
                                                                 }];
                                            } else {
                                                weakAlbumCell.albumImageView.alpha = 1.0;
                                            }
                                        });
                                    } usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];

    return albumCell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self performSegueWithIdentifier:@"showTracks" sender:nil];
}
@end
