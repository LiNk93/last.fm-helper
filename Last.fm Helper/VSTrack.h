//
//  VSTrack.h
//  Last.fm Helper
//
//  Created by Vladislav Solovyov on 16.12.15.
//  Copyright © 2015 Astarus. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Mantle/Mantle.h>
#import "MTLModel.h"

@interface VSTrack : MTLModel <MTLJSONSerializing>

@property (copy, nonatomic) NSString *name;

@end
