//
//  VSAlbum.m
//  Last.fm Helper
//
//  Created by Vladislav Solovyov on 16.12.15.
//  Copyright © 2015 Astarus. All rights reserved.
//

#import "VSAlbum.h"
#import "VSArtist.h"
#import "VSImage.h"

@implementation VSAlbum

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{@"name": @"name",
             @"albumUrl": @"url",
             @"images": @"image",
             @"artist": @"artist",
             @"mbid": @"mbid"};
}

+ (NSValueTransformer *)urlJSONTransformer {
    return [MTLValueTransformer transformerUsingForwardBlock:^id (NSString *str, BOOL *success, NSError *__autoreleasing *error){
        NSString *decodedString = [str stringByRemovingPercentEncoding];
        return [NSURL URLWithString:decodedString];
    } reverseBlock:^id (NSURL *url, BOOL *success, NSError *__autoreleasing *error){
        NSString *urlStr = [url absoluteString];
        NSString *encodedString = [urlStr stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
        return encodedString;
    }];
}

+ (NSValueTransformer *)albumUrlJSONTransformer {
    return [NSValueTransformer valueTransformerForName:MTLURLValueTransformerName];
}

+ (NSValueTransformer *)artistJSONTransformer {
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:[VSArtist class]];
}

+ (NSValueTransformer *)imagesJSONTransformer {
    return [MTLJSONAdapter arrayTransformerWithModelClass:[VSImage class]];
}

@end
