//
//  MasterViewController.h
//  Last.fm Helper
//
//  Created by Vladislav Solovyov on 15.12.15.
//  Copyright © 2015 Astarus. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ArtistTopAlbumsViewController;

@interface SearchArtistViewController : UITableViewController

@property (strong, nonatomic) ArtistTopAlbumsViewController *artistTopAlbumsViewController;


@end

